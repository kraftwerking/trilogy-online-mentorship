# Session 2

## What students should have accomplished before this session

### Lessons
* Nomster: Lesson 1-12

### Assignments
* Blogging about Code

### Videos
* Model-View-Controller Overview Videos

## Overview of the Session

This session should cover these basics

* **Introduction**
* **Overview** with the student—how they're coming along and how they're feeling about the previous week
* **Recap** of the major touch points they learned
* **Questions** students may have from that week
* **Lesson** for the student relevant to their level and process (time permitting)

## Session Plan

### **Before Starting:** Clock-In with ADP

* Make sure to clock-in before starting your session on ADP. For more on how to clock-in and out, please see the [Post-Session Guide](../../../01-Important-Docs/Post-Session\ Guide.md).

### Introductions (5 minutes)

#### Small Talk

Start off with asking the student how they are doing in general. This is your chance to build continuous rapport with your students.

### Overview (5 minutes)

* Start off asking your student what questions they have. *You aren't going to dive into these right now, but this gives you time to think about them and/or incorporate them into the recap. Write these questions down as your student relays them to you.*

> "What questions do you have this week?"

* Next, ask how far students were able to get. *Take notes on their progress.*

> "How far were you able to get since last week?"

* Then ask about their general feelings from this lesson, including pain points. *Take notes on their strong points, interests, and trouble spots.*

> "How did feel about this week's lessons?"

### Recap (15 minutes)

Give a quick lecture to recap of the important parts of this unit:

* Setting up a Ruby on Rails project using Rake
* The importance of wireframing
* Setting up a new database using Rake
* Adding Bootstrap to the project using the Bootstrap Gem and what Bootstrap is
* Adding pagination using a pagination Gem

### Questions (35 minutes)

Dive into your student's questions. This is a crucial time to answer pressing questions that might be hindering their progress and also help them better understand things that might not be clicking.

Have students share their screens and step you through the problems. Then do paired-programming to walk the student through writing their own code.

### Lesson (Remaining Time)

Lessons for this session are coming soon.

* Alternatives to teaching this session's lesson include:
    * Pair programming
    * Introduce a new topic
    * Review coding challenges

### Wrap Up

Wrap up any lingering questions or points you would like to make.

**Schedule your next mentorship session and send a Google Calendar invite before the end of the session.**

## Post-Session

### Reflection

How did today's session go?

[Went Well](https://www.surveygizmo.com/s3/4898621/Trilogy-Online-Feedback?sentiment=positive&lesson=2)

[Went Poorly](https://www.surveygizmo.com/s3/4898621/Trilogy-Online-Feedback?sentiment=negative&lesson=2)

[Submit an Issue](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new)

### Problems in the Session, Lesson, or Feedback

* General feedback (positive or negitive): [Mentorship Curriculum Feedback Google Form](https://forms.gle/p4zQPmZii6g9S48e8)
* Specific issues with lesson plans (LPs) or session plans (SPs): [Submit an issue in the `trilogy-online-mentorship` GitHub repo](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new)
* General questions or open discussions: [`#mentors-discussion` on the Awesome Tutors & Mentors Slack workspace](https://awesometutors.slack.com).

### Send Calendar Invite

Send your mentee a calendar invite on Google Calendar for the time and date you agreed upon, including your Zoom Room link.

### Go to Bootcamp Spot and Log the Session

To log a session, go to the [Mentor Dashboard](https://online.bootcampspot.com/mentor/dashboard) located on Bootcamp Spot. Select "Log Session".

![Log a Student in BCS](../../../images/log-student.png)

In your log to the student, include the key points you went over, any tips you'd like to pass on for their next week, and include the Panopto recording from [the Panoptop website](https://codingbootcamp.hosted.panopto.com).

**Note: The top input box in the Log Session page is public and is shared with the student. The bottom input box is private and is just shared with the bootcamp staff. Your student only sees the top input box.**

**Note: Panopto recordings can take a little bit to appear on the site.**

When going to the Panopto website, login, and go to the "Browse" option on the left-hand sidebar menu. Select the "All Folders" tab in the window that appears. Type "zoom-pro" in the search field and then find your username in the list. It will hold all your session videos.

![Panopto](../../../images/panopto-ss.png)

### Log hours on ADP

Take a look at the [Post-Session Guide](../../../Post-Session\ Guide.md) document on instructions on how to submit hours to ADP or [read this Google Doc.](https://docs.google.com/document/d/1xYRJrshiXEBmh8icXcDoXpsdXqL-hZq351h8fFfUEm8])