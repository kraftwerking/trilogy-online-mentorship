# Session 15

## What students should have accomplished before this session

### Lessons
* SPA: Lessons 11-20
* Complete SPA

### Challenges
* Build the Join Method
* Kata Challenges
* Build the Map Method

### Team Project
* First team meeting

## Overview of the Session

This session should cover these basics

* **Introduction**
* **Overview** with the student—how they're coming along and how they're feeling about the previous week
* **Recap** of the major touch points they learned
* **Questions** students may have from that week
* **Lesson** for the student relevant to their level and process (time permitting)

## Session Plan

### **Before Starting:** Clock-In with ADP

* Make sure to clock-in before starting your session on ADP. For more on how to clock-in and out, please see the [Post-Session Guide](../../../01-Important-Docs/Post-Session\ Guide.md).

### Introductions (5 minutes)

#### Small Talk

Start off with asking the student how they are doing in general. This is your chance to build continuous rapport with your students.

### Overview (5 minutes)

* Start off asking your student what questions they have. *You aren't going to dive into these right now, but this gives you time to think about them and/or incorporate them into the recap. Write these questions down as your student relays them to you.*

> "What questions do you have this week?"

* Next, ask how far students were able to get. *Take notes on their progress.*

> "How far were you able to get since last week?"

* Then ask about their general feelings from this lesson, including pain points. *Take notes on their strong points, interests, and trouble spots.*

> "How did feel about this week's lessons?"

### Team Project Introduction (5 minutes)

Talk to your student about the team project. Touch on the following things:

* They will be writing a chess game with other students from the university.
* They will be meeting weekly with a team mentor who will help keep the project on track.
* The mentor assigned to the team may or may not be you (their current mentor).
* They will still have regular mentor meetings with you (their current mentor).
* They will need to use GitHub to collaborate with their team, this includes branching, merging, and creating pull requests.

### Questions (45 minutes)

Dive into your student's questions. This is a crucial time to answer pressing questions that might be hindering their progress and also help them better understand things that might not be clicking.

Have students share their screens and step you through the problems. Then do paired-programming to walk the student through writing their own code.

### Lesson (Remaining Time)

Lessons for this session are coming soon.

* Alternatives to teaching this session's lesson include:
    * Pair programming
    * Introduce a new topic
    * Review coding challenges

### Wrap Up

Wrap up any lingering questions or points you would like to make.

**Schedule your next mentorship session and send a Google Calendar invite before the end of the session.**

## Post-Session

### Reflection

How did today's session go?

[Went Well](https://www.surveygizmo.com/s3/4898621/Trilogy-Online-Feedback?sentiment=positive&lesson=15)

[Went Poorly](https://www.surveygizmo.com/s3/4898621/Trilogy-Online-Feedback?sentiment=negative&lesson=15)

[Submit an Issue](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new)

### Problems in the Session, Lesson, or Feedback

* General feedback (positive or negitive): [Mentorship Curriculum Feedback Google Form](https://forms.gle/p4zQPmZii6g9S48e8)
* Specific issues with lesson plans (LPs) or session plans (SPs): [Submit an issue in the `trilogy-online-mentorship` GitHub repo](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new)
* General questions or open discussions: [`#mentors-discussion` on the Awesome Tutors & Mentors Slack workspace](https://awesometutors.slack.com).

### Post-Session Guide

See the [Post-Session Guide.md](../../../01-Important-Docs/Post-Session\ Guide.md) file for a complete guide on what to do post-session.