puts "Enter a number:"
num = gets.chomp.to_i

num_lop = num % 10

case num_lop
when 1
    suffix = "st"
when 2
    suffix = "nd"
when 3
    suffix = "rd"
else
    suffix = "th"
end

ordinal = num.to_s + suffix

if num.between?(11, 13)
    puts "That's the #{num}th item!"
else 
    puts "That's the #{ordinal} item!"
end