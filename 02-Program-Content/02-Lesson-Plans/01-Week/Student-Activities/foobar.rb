puts "How many items do you want to see?"

items = gets.chomp.to_i

list = []

(1..items).each do |n|
    if n % 3 == 0 && n % 5 == 0
        list << "foobar"
        elsif n % 3 == 0
        list << "foo"
        elsif n % 5 == 0
        list << "bar"
    else
        list << n
    end
end

puts list