# Trilogy Online Mentorship Plans and Resources

Welcome to the Trilogy Online mentorship repository! We are super glad to have you a part of the team and look forward to your journey with us. 

This repository is designed to follow the 24 week program structure outlined [here](./images/timeline24WeekProgram.png). 

## This repository includes:

* Session Plans
    * Guided plans for each individual mentorship session.
* Lesson Plans
    * Guided lessons that can be used during a session.
* Applications from the Curriculum
    * Application source for the applications that students build during the course.

## Structure



```
trilogy-online-mentorship
|
|---- 03-Project-Source-Code
|   |
|   |---- Splurty (Nomster, etc..)
|   |
|---- 02-Program-Content
|   |
|   |---- 01-Session-Plan
|   |   |
|   |   |---- XX-Week
|   |   |   |
|   |   |   |---- XX-Session.md
|   |   |
|   |---- 02-Lessons
|   |   |
|   |   |---- XX-Week
|   |   |   |
|   |   |   |---- Lesson-Files
|   |   |   |---- Student-Activities
|   |   |   |---- XX-Lesson.md
|   |   |
|---- 01-Important-Docs
|   |
|   |---- File.md

```

The `01-Important-Docs` directory includes important documents for how to go about the mentorship program such as how sessions are set up and what to do after a session. Please read all the documents in this directory.

The `02-Program-Content` directory includes content for the mentorship program itself. This includes `01-Session-Plans` which is broken up by week and includes the ideal plan for each individual session and `02-Lessons` which includes mini-lessons that you can teach during a mentorship session if the student's questions don't fill up the entire session as well as the source code for small activities the student might have done that week.

The `03-Project-Source-Code` directory includes the source code for all the major applications students develop during the course.

**Warning**: Because of the async nature of the program, you may find yourself jumping between weeks for content depending on the students progress during the prior week. Try your best to encourage students to keep on track!

## Contributions

We love all contributions! If you have feedback, or if there is anything you would like to add, or a feature you think will make your job easier please let us know.

* General feedback (positive or negitive): [Mentorship Curriculum Feedback Google Form](https://forms.gle/p4zQPmZii6g9S48e8)
* Specific issues with lesson plans (LPs) or session plans (SPs): [Submit an issue in the `trilogy-online-mentorship` GitHub repo](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new)
* General questions or open discussions: [`#mentors-discussion` on the Awesome Tutors & Mentors Slack workspace](https://awesometutors.slack.com).