# Trilogy Online Mentorship To Do

The following are things that are in-progress to be done to this curriculum.

Please submit issues with your comments and feedback.

## General

* Include an overview of the types of students this course works with

## Session Plans

* Incorporate job prep and career services

## Lesson Plans

* Write lesson plans for sessions 1-15 and 22-23