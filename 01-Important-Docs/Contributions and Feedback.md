# Mentorship Curriculum Contributions and Feedback

## General Curriculum Feedback

If you have general feedback about how the mentorship sessions and lessons are going, please let us know in the [Mentorship Curriculum Feedback Google Form](https://forms.gle/p4zQPmZii6g9S48e8).

## Problems in the Lesson or Session

If you encounter any problems in the session plans (SPs) or lesson plans (LPs), please [submit an issue in the `trilogy-online-mentorship` GitHub repo](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new).

## Discussion with Other Mentors

If you have questions or if you would like to have an open discussion about the mentorship curriculum, session plans (SPs), or lesson plans (LPs), please use the `#mentors-discussion` channel on the [Awesome Tutors & Mentors Slack workspace](https://awesometutors.slack.com).