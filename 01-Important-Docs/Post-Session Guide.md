# Mentor Guidelines for Post-Session

## Reflection

At the bottom of each session plan is a reflection section where you can quickly log whether the session went well or not. This helps us improve the sessions. 

## Problems in the Lesson

If you encounter any problems in the session plans or lesson plans, please [submit an issue in the `trilogy-online-mentorship` GitHub repo](https://github.com/coding-boot-camp/trilogy-online-mentorship/issues/new).

## Send Calendar Invite

Send your mentee a calendar invite on Google Calendar for the time and date you agreed upon, including your Zoom Room link.

## Go to Bootcamp Spot and Log the Session

To log a session, go to the [Mentor Dashboard](https://online.bootcampspot.com/mentor/dashboard) located on Bootcamp Spot. Select "Log Session".

![Log a Student in BCS](../images/log-student.png)

In your log to the student, include the key points you went over, any tips you'd like to pass on for their next week, and include the Panopto recording from [the Panoptop website](https://codingbootcamp.hosted.panopto.com).

**Note: The top input box in the Log Session page is public and is shared with the student. The bottom input box is private and is just shared with the bootcamp staff. Your student only sees the top input box.**

**Note: Panopto recordings can take a little bit to appear on the site.**

When going to the Panopto website, login, and go to the "Browse" option on the left-hand sidebar menu. Select the "All Folders" tab in the window that appears. Type "zoom-pro" in the search field and then find your username in the list. It will hold all your session videos.

![Panopto](../images/panopto-ss.png)

## Log Hours on ADP

### Important Notes

If a student's institution **is not** listed as "FIREHOSE"

- You will use ADP to log sessions.
- You will complete Mentor Session Notes for each session.
- You will be paid according to the information explained below.

If a student's institution **is** listed as "FIREHOSE"

- You will complete Mentor Session Notes for each session.
- You will be paid based on logged mentor session notes every six weeks.


### General Information

ADP should be able to help if you have ADP platform questions. Their number is: `844-711-2094`

If you have payroll questions, e-mail `payroll@trilogyed.com` and CC `centraltutorsupport@bootcampspot.com`.

### How to Use ADP

- Log into ADP at [https://workforcenow.adp.com](workforcenow.adp.com)

![ADP](../images/adp1.png)

- Click **HOME**

![ADP](../images/adp2.png)

- Click **MY TIMECARD**

![ADP](../images/adp3.png)

- Make sure you are using the **MEN - Mentor** profile if you have more than one position with Trilogy such as a Teaching Assistant (TA) or Tutor

![ADP](../images/adp4.png)

- **CLOCK IN** at the start of each session

![ADP](../images/adp5.png)

- **CLOCK OUT WITH NOTES** at the end of each session

![ADP](../images/adp6.png)

- Make sure to **SAVE** your notes

### Clock-Out Notes Field

Add notes by using the **Clock Out with Notes** button after each session concludes. Include the following notes:

- Course cohort
- Student name
- B2B-Yes or B2B-No to indicate whether or not the session was a back-to-back session
- No-show (only if the elapsed time is 10 minutes or less)

### Back-to-Back Session

By definition on our team, a Back-to-Back session is 2 sessions with the same student one right after another. This means you clock in once at the beginning and clock out at the end of the 2 hours.

### Sessions in Succession

If you have students scheduled one after the other, you will need to have at least 2 minutes between clocking out and clocking in for the next session.  ADP requires at least 2 minutes minimum between clock-out/clock-in.

[A more detailed guide to ADP can be found in this Google Doc.](https://docs.google.com/document/d/1xYRJrshiXEBmh8icXcDoXpsdXqL-hZq351h8fFfUEm8])