# Mentor Guidelines for Mentor Sessions

## Breakdown of Different Assignments Given to Students
* There are a variety of different types of assignments that students will be doing over the course of the bootcamp. Some assignments are handled differently than others.
    * **Pre-Work** is graded by TAs.
    * **Apps (ie. Splurty, Nomster)** are graded by TAs.
    * **Challenges (like Ordinal Challenge and Foobar Challenge)** are graded by TAs.
    * **Non-Named Challenges (like Image Blur)** are reviewed by mentors.
    * **Videos** are watched by a student.
    * **Lessons** are completed by the student.
    * **Quizzes** are shared via profiles.

## Mentor Responsibilities During Sessions
* Check student progress with timeline (remind students to use timeline to guide their progress).
* Answer student questions, use probing questions to help students find their own answers. Questions are the driving force of sessions, expect the majority of the session time to be spent on questions. Take the opportunity during questions to dive deeper into the concepts behind a student's question.
* Utilize the remaining time in the best way possible for the student (pair programming, resume review, previewing new content, associated session lesson, etc.).
* Assign homework when applicable, reminding the student to continue the curriculum. (Not all sessions will have additional homework.)
* Schedule next mentor session.
* Take notes on the session and submit them in the Mentor Dashboard. ([See Post-Session Guide](Post-Session\ Guide.md))

## Student Responsibilities During Sessions
* Actively participate in session.
* Come prepared with list of questions and e-mail questions to their mentor 24 hours ahead of time with some or all of those questions.
* Ask questions when mentor's explanations are unclear.
* Schedule next mentor session.

## Mentor Session Activities
* There are a variety of activities that you may choose from to complete during the mentor sessions. Questions are the main part of each session and require the most time. Pick from this list something to do if questions did not fill up the entire session.

#### Answering Questions 
Students should bring a list of questions to the mentor sessions; if they don't, suggest they do so for the next session. Start each session with student questions to ensure you get through them all.
 
Have your mentee e-mail you questions 24 hours ahead of time. They should also have a notebook or document on their computer to keep a running list of questions. If they do not, suggest they do this. Every time they come across something they don't understand or every time they get stuck on something, they should make a note of it.

#### Reviewing Coding Challenges
Students will be working on their coding challenges during the week. It is useful to review their code. After you review the code you may want to ask questions or give suggestions on how to proceed.

#### Introducing a New Topic
As students complete a challenge or app, you may want to review the next assignment and explain higher level concepts before they begin. This can be useful for some topics. For example before giving an assignment on integrating with the Google Place API (used in Nomster) an explanation of the following topics is useful: What is JSON?;  What are APIs?; How do you connect to APIs?; What is HTTParty and why are we choosing it over the Ruby Standard Library?

#### Pair Programming
Pair programming to add a feature to any of their web apps, begin a new web app, or solve an algorithm can be a very useful learning experience for students. Be sure to take turns driving and giving directions when pair programming.

#### Laying Out the Next Lesson/Track
Help guide students to complete their work by talking about what is due for the next session. You can also give a quick demo or lesson on what they will be doing next.

