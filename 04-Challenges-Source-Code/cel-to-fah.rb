# degrees Celsius * 9/5 plus 32 = Fahrenheit

def degrees (c)
    return c * 9 / 5 + 32
end

puts "*** Celsius to Fahrenheit Conversion ***"
puts
puts "Please enter the degrees in Celsius:"
num = gets.chomp.to_f

num_converted = degrees(num)
result = "#{num} Celsius = #{num_converted} Fahrenheit!"

puts result